const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: ['true', 'Title is required.']
    },
    minAge: {
        type: Number,
        required: ['true', 'Age is required.']
    },
    maxAge: {
        type: Number,
        required: ['true', 'Age is required.']
    },
    price: {
        type: Number,
        required: ['true', 'Price is required.']
    },
    link: {
        type: String,
        required: ['true', 'Link is required']
    },
    interests: {
        type: Array,
        required: ['true', 'Interests is required.']
    },
    description: {
        type: String,
        default: ''
    }
});

module.exports = mongoose.model( 'Gift', Schema );