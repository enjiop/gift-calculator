const Router = require('koa-router');
const router = new Router();
const GiftApi = require('../api/gift');

router.get('/gift/all', async (ctx) => {
    try {
        const result = await GiftApi.getAllGifts();
        ctx.status = 200;
        ctx.body = result;
    } catch (error) {
        ctx.status = 500;
        ctx.body = error || 'Iternal Error';
    }
});

router.get('/gift/:giftId', async (ctx) => {
    try {
        const result = await GiftApi.getGift(ctx.params.giftId);
        ctx.status = 200;
        ctx.body = result;
    } catch (error) {
        ctx.status = 500;
        ctx.body = error || 'Iternal Error';
    }
});

router.post('/createGift', async (ctx) => {
    try {
        const result = await GiftApi.createGift({...ctx.request.body});
        ctx.status = 201;
        ctx.body = result;
    } catch (error) {
        ctx.status = 500;
        ctx.body = error || 'Iternal Error';
    }
});

router.delete('/gift/:giftId', async (ctx) => {
    try {
        const result = await GiftApi.deleteGift(ctx.params.giftId);
        ctx.status = 200;
        ctx.body = result;
    } catch (error) {
        ctx.status = 500;
        ctx.body = error || 'Iternal Error';
    }
});

router.post('/sort', async (ctx) => {
    try {
        const result = await GiftApi.Sort({...ctx.request.body});

        console.log({...ctx.request.body})
        ctx.status = 200;
        ctx.body = result;
    } catch (error) {
        ctx.status = 500;
        ctx.body = error || 'Iternal Error';
    }
});

module.exports = router;