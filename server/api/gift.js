const Gift = require('../database/model/gift');

exports.createGift = ({title, minAge, maxAge, price, interests, description, link }) => new Promise( async (resolve, reject) => {
    try {
        if ( (minAge === 0) && (maxAge === 0) ) {
            maxAge = 9999;
        }
        console.log('Data in API: ', title, minAge, maxAge, link, price, interests, description);
        const newGift = new Gift({
            title, 
            minAge,
            maxAge,
            link, 
            price, 
            interests, 
            description
        });

        const createdGift = await newGift.save();
        resolve({
            success: true,
            data: createdGift
        });
    } catch (error) {
        reject(error);
    }
});

exports.getAllGifts = () => new Promise( async (resolve, reject) => {
    try {
        const allGifts = await Gift.find();

        resolve({
            success: true,
            data: allGifts
        });
    } catch (error) {
        reject(error);
    }
});

exports.getGift = (_id) => new Promise( async (resolve, reject) => {
    try {
        const singleGift = await Gift.findById(_id);
        resolve({
            success: true,
            data: singleGift
        });
    } catch (error) {
        reject(error);
    }
});

exports.deleteGift = (_id) => new Promise( async (resolve, reject) => {
    try {
        const removeGift = await Gift.deleteOne({ _id });
        resolve({
            success: true,
            data: removeGift
        });
    } catch (error) {
        reject(error);
    }
});

exports.Sort = (  { interests, ratio, age }  ) => new Promise( async (resolve, reject) => {
    try {
        console.log(interests, ratio, age);
        
        let sortedGifts;
        if(ratio > 2) {
            sortedGifts = await Gift.find({ minAge: { $lte: age }, maxAge: { $gte: age } }).all('interests', interests).sort('-price')
        } else {
            sortedGifts = await Gift.find({ minAge: { $lte: age }, maxAge: { $gte: age } }).all('interests', interests).sort('price');
        }
        resolve({
            success: true,
            data: sortedGifts
        });
    } catch (error) {
        reject(error);
    }
});
