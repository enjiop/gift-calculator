const interests = ['Технологии', 'Игрушки', 'Активный отдых', 'Алкоголь', 'Гаджеты', 'Музыка', 'Спорт', 'Одежда', 'Книги', 'Еда'];

let selectedIntersts = [];

class InterestsComponent {
    constructor(node) {
        this.render(node);
    }

    addSelectedInterest( checkbox ) {
        selectedIntersts.push(checkbox.value);
    }

    deleteUnselectedInterest( checkbox ) {
        selectedIntersts.forEach( (item, i) => {
            if(item === checkbox.value) {
                selectedIntersts.splice(i, 1);
            }
        });
    }

    get selectedItems() {
        return selectedIntersts;
    }

    set selectedItems(value) {
        selectedIntersts = value;
    }

    handleCheckboxChange = (e) => {
        const target = e.target;
        if(target.checked) {
            this.addSelectedInterest(target);
        } else {
            this.deleteUnselectedInterest(target);
        }

        console.log(selectedIntersts);
    }

    render(node) {
        interests.map( item => {
            const interestNode = document.createElement('label');
            interestNode.innerHTML = `
                <input type="checkbox" name="interest" value="${item}"/>
                <span>${item}</span>
            `;

            interestNode.querySelector('[name="interest"]').addEventListener('change', this.handleCheckboxChange);
            node.appendChild(interestNode);
        });

    }
}

export default InterestsComponent;