import {
    Observable,
    Observer
} from './../../tools/observer'

let Cart = [];

class CartComponent {
    constructor() {
        this.observable = new Observable();

        this.cartObs = new Observer(( { id, data } ) => {
            const filteredToBasket = data.find(item => item._id === id);
            Cart.push(filteredToBasket);
            this.renderBasket();
        });

        this.iconObs = new Observer(({ id }) => {
            let gifts__cart = document.getElementById('liked-gifts__cart');
            gifts__cart.textContent = Cart.length;
        });

        this.observable.addObserver(this.cartObs);
        this.observable.addObserver(this.iconObs);
    }

    get Cart() {
        return Cart;
    }

    renderBasket() {
        let cartElem = document.getElementById('cart');
        let message;

        if (Cart.length === 0) {
            message = 'Пока пусто:(';
        } else {
            let Sum = Cart.reduce((prev, current) => {
                return prev += Number(current.price);
            }, 0);
            message = `У вас в избранном ${Cart.length} товаров, на сумму: ${Sum} грн.`;
        }

        cartElem.innerHTML = `<h2>${message}</h2><ol></ol>`;

        let ol = cartElem.querySelector('ol');
        Cart.map(item => {
            let li = document.createElement('li');
            li.innerText = `${item.title} (${item.price} грн.)`;
            ol.appendChild(li);
        });
    }
}

export default CartComponent;