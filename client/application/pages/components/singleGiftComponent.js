class SingleGiftComponent {
    constructor(node, gift) {
        this.render(node, gift);
    }

    ageString(minAge, maxAge) {
        return (minAge === 0) && (maxAge === 9999) ? 'Для всех' : `От ${minAge} до ${maxAge}`;
    }

    interestsString(interests) {
        return interests.join(', ');
    }

    render(node, gift) {
        node.innerHTML =
        `
            <div class="gift-name__wrap">
                <h3 class="gift__name">${gift.title}</h3>
            </div>
            <span class="gift__age">${this.ageString(gift.minAge, gift.maxAge)}</span>
            <span class="gift__price">${gift.price} грн.</span>
            <span class="gift__interests">${this.interestsString(gift.interests)}</span>
            <p class="gift__description">${gift.description}</p>
            <a class="gift__link" href="${gift.link}">Купить по ссылке</a> 
		    <div class="gift__action">
		    	<button class="btn gift__buy" data-id=${gift._id}> В избранное </button>
            </div>
        `;
    }
}

export default SingleGiftComponent;