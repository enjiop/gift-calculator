import FormComponent from './formComponent';
import { redirectTo } from './../../../tools/router'
import Gift from '../../../models/Gift';

class GetGiftForm extends FormComponent {
    constructor(node, template) {
        super();
        this.render(node, template);
    }

    toLocalStorageData(data) {
        let validateData = JSON.stringify(data.map(item => new Gift(item)));
        localStorage.setItem('recomendedData', validateData);
    }

    fetchData(data, path) {
        fetch(`http://localhost:5000${path}`, {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => {
                this.toLocalStorageData([...res])
                redirectTo('/recomended');
            });
    }

    parseForm(form , interests) {
        let data = super.parseForm(form, interests);
        data.ratio = form.rating.value;
        console.log(form.rating.value)
        data.age = form.age.value;

        return data;
    }

    render( node, template ) {
        const form = super.render(node, template, '/sort');

        return form;
    }
}

export default GetGiftForm;