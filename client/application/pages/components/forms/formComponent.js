import InterestsComponent from "../interestsComponent";
import {
    redirectTo
} from '../../../tools/router';

class FormComponent {
    handleSubmit = ( interests, path ) => (e) => {
        e.preventDefault();
        let data = this.parseForm(e.target, interests);
        this.fetchData(data, path);
    }

    parseForm( form, interests ) {
        let data = {};
        let inputs = Array.from(form.elements).filter(item => item.type !== 'checkbox' );
        inputs.map( item => {
            if(item.type === 'number') {
                data[item.name] = +item.value;
            } else {
                data[item.name] = item.value;
            }
        });

        data.interests = interests.selectedItems;

        interests.selectedItems = [];

        return data;
    }

    fetchData(data, path) {
        console.log(data);
        fetch(`http://localhost:5000${path}`, {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(res => {
                console.log(res)
                redirectTo('');
            });
    }

    render(node, template, path) {
        const form = document.createElement('form');
        form.name = 'getGiftForm';
        form.innerHTML = template;

        const interests = new InterestsComponent(form.querySelector('[name="interests"]'));
        form.addEventListener('submit', this.handleSubmit(interests, path));
        node.appendChild(form);

        return form;
    }
}

export default FormComponent;