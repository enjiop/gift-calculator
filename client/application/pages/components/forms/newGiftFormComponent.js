import FormComponent from './formComponent';

class NewGiftFormComponent extends FormComponent {
    constructor(node, template) {
        super();
        this.render(node, template);
    }

    handleChangeAllAge = ( form ) => (e) => {
        if(e.target.checked) {
            form.minAge.disabled = true;
            form.maxAge.disabled = true;
            form.minAge.value = null;
            form.maxAge.value = null;
        } else {
            form.minAge.disabled = false;
            form.maxAge.disabled = false;
        }
    }

    render(node, template) {
        const form = super.render(node, template, '/createGift');
        form.allAges.addEventListener('change', this.handleChangeAllAge( form ));
    }
};

export default NewGiftFormComponent;