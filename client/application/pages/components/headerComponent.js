import {
    renderRoute,
    pushToHistory,
    getRouteByPath
} from './../../tools/router'
import CartComponent from './cartComponent';

class HeaderComponent {
    constructor(node) {
        this.cart = new CartComponent();
    }

    handleShowBasket = (e) => {
        let cartElem = document.getElementById('cart');
        cartElem.classList.toggle('show');
    }

    render(node) {
        const header = document.createElement('header');
        header.className = 'header';

        header.innerHTML = `
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-3 logo">
                        <a class="col logo__link" href="#">presenty</a>
                    </div>
                    <nav class="main-nav col-6 order-1">
                        <button class="btn _toGetGift">Подобрать подарок</button></li>
                        <button class="btn new_btn _toNewGift">Добавить подарок</button></li>
                    </nav>
                    <div class="col-3 liked-gifts__icon order-last" id="liked-gifts__icon">
			            <button class="liked-gift__button"><img src="https://img.icons8.com/material-outlined/24/000000/filled-like.png" /></button>
			            <span id="liked-gifts__cart">${this.cart.Cart.length}</span>
		            </div>
                </div>
                
            </div>
        `;

        const likedgiftbutton = header.querySelector('.liked-gift__button');
        likedgiftbutton.addEventListener('click', this.handleShowBasket);

        const toGetGiftBtn = header.querySelector('._toGetGift');
        const toNewGiftBtn = header.querySelector('._toNewGift');

        toGetGiftBtn.addEventListener('click', this.handleTransitionToPage(getRouteByPath('/getGift')));
        toNewGiftBtn.addEventListener('click', this.handleTransitionToPage(getRouteByPath('/create')));

        const cartContainerNode = document.createElement('div');
            cartContainerNode.className = "gifts__cartContainer"
            cartContainerNode.id = 'cart';

            node.appendChild(header);
            node.appendChild(cartContainerNode);

        this.cart.renderBasket();
        return header;
    }

    handleTransitionToPage = (route) => (e) => {
        pushToHistory(route);
        renderRoute(route);
    }
}

export default HeaderComponent;