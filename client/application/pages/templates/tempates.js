export const newGiftFormTemplate = `
    <label>
        <span>Название подарка<span class="required">*</span></span>
        <input type="text" class="" name="title" required/>
    </label>

    <label>
        <span>Возраст:<span class="required">*</span></span>

        <label>
            <input type="checkbox" name="allAges"/>
            <span>Все возраста</span>
        </label>

        <label>
            <span>От</span>
            <input type="number" class="" name="minAge" min="0" required/>
        </label>

        <label>
            <span>До</span>
            <input type="number" class="" name="maxAge" required/>
        </label>
    </label>

    <label>
        <span>Цена (грн)<span class="required">*</span>:</span>
        <input type="number" class="" name="price" min="0" required/>
    </label>

    <label>
        <span>Ссылка где купить<span class="required">*</span>:</span>
        <input type="text" name="link" required/>
    </label>

    <label name="interests">

    </label>

    <textarea type="text" class="" style="resize: none" name="description"></textarea>
    <button>Создать подарок</button>
`;

export const getGiftFormTemplate = `
    <label>
        <span>Имя:</span>
        <input type="text" name="name" />
    </label>
    <label>
        <span>Возраст:<span class="required">*</span></span>
        <input type="number" name="age" required />
    </label>
    <label>
        <fieldsetc class="rating">
            <legend class="rating__caption">Отношение к другу</legend>
            <div class="rating__group">   
                <input type="radio" value="1" class="rating__star" name="rating" checked/>
                <input type="radio" value="2" class="rating__star" name="rating" />
                <input type="radio" value="3" class="rating__star" name="rating" />
                <input type="radio" value="4" class="rating__star" name="rating" />
                <input type="radio" value="5" class="rating__star" name="rating" />
                <div class="rating__focus"></div>
            </div>
        </fieldsetc>
    </label>
    <label name="interests">
        <span>Интересы<span class="required">*</span>:</span>
    </label>
    <button>Найти подарок</button>
`;