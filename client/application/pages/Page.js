import HeaderComponent from './components/headerComponent'

import {
    Observable,
    Observer
} from './../tools/observer'
class Page {
    constructor(root, title) {
        this.title = title;
        this.root = root;
        this.header = new HeaderComponent(this.root);
        this.cart = this.header.cart;
        
        this.root.innerHTML = null;
    }
    
    render() {
        this.header.render(this.root);
        const contentNode = document.createElement('div');
            contentNode.className = 'container';

        const titleNode = document.createElement('h2');
        titleNode.className = 'page-title'
        titleNode.textContent = this.title;

        contentNode.appendChild(titleNode);
        this.root.appendChild(contentNode);

        return contentNode;
    }
}

export default Page;