import NewGiftFormComponent from "./components/forms/newGiftFormComponent";
import Page from "./Page";
import { newGiftFormTemplate } from './templates/tempates';

export class NewGiftPage extends Page {
    constructor(node, title) {
        super(node, title);
        this.render();
    }

    render() {
        const node = super.render();
        new NewGiftFormComponent(node, newGiftFormTemplate);

        return node;
    }
};