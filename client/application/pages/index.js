export * from './HomePage';
export * from './NewGiftPage';
export * from './RecomendedGiftsPage';
export * from './GetGiftPage';