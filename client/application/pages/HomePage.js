import {
    getRouteByPath,
    renderRoute,
    pushToHistory
} from '../tools/router';
import Page from './Page';
import SingleGiftComponent from './components/singleGiftComponent';
export class HomePage extends Page {
    constructor(node, title) {
        super(node, title);

        this.giftsRowNode = document.createElement('div');
        this.giftsRowNode.className = 'row justify-content-beetween';
        this.giftsRowNode.id = "gifts__row";

        this.render();
    }

    fetchData(node) {
        fetch('http://localhost:5000/gift/all')
            .then(res => res.json())
            .then(res => {
                this.renderList(res.data, node);
            });
    }

    renderList(data) {
        data.map( item => {
            let gift = document.createElement('div');
            gift.className = "gift col-md-3 col-sm-5";

			new SingleGiftComponent(gift, item);
			let buyButton = gift.querySelector('.gift__buy');
				buyButton.addEventListener('click', (e) => {
                let id = e.target.dataset.id;
                this.cart.observable.sendMessage({ id, data });
			});
			this.giftsRowNode.appendChild(gift);
        });
    }

    render() {
        const node = super.render();

        this.fetchData(this.giftsRowNode);
        node.appendChild(this.giftsRowNode);
    }
};