import Page from "./Page";
import {
    redirectTo
} from '../tools/router'
import SingleGiftComponent from './components/singleGiftComponent'
export class RecomendedGiftsPage extends Page {
    constructor(node, title) {
        super(node, title);

        this.data = JSON.parse(localStorage.getItem('recomendedData'));
        localStorage.removeItem('recomendedData');

        this.giftsRowNode = document.createElement('div');
        this.giftsRowNode.className = 'container gifts__row';
        this.giftsRowNode.id = "gifts__row";

        this.render();
    }

    renderList(data) {
        if (data === null || data.length === 0) {
            this.giftsRowNode.innerHTML = `
                <h3>Сожалеем, по вашому запросу ничего не нашлось</h3>
                <button class="btn _toGet">Попробовать еще раз</button>
            `;
            this.giftsRowNode.querySelector('._toGet').addEventListener('click', () => {
                redirectTo('/getGift');
            });
        } else {
            data.map(item => {
                let gift = document.createElement('div');
                gift.className = "gift col-md-3 col-sm-5";

                new SingleGiftComponent(gift, item);

                let buyButton = gift.querySelector('.gift__buy');
                buyButton.addEventListener('click', (e) => {
                    let id = e.target.dataset.id;
                    console.log(id, data)
                    this.cart.observable.sendMessage({
                        id,
                        data
                    });

                    console.log(this.cart.Cart);
                });
                this.giftsRowNode.appendChild(gift);
            });
        }
    }

    render() {
        const node = super.render();
        node.appendChild(this.giftsRowNode);
        console.log(this.data);
        this.renderList(this.data);

        return node;
    }
};