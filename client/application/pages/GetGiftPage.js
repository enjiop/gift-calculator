import Page from "./Page";
import GetGiftForm from './components/forms/getGiftForm';
import { getGiftFormTemplate } from "./templates/tempates";

export class GetGiftPage extends Page {
    constructor(node, title) {
        super(node, title);
        this.render();
    }

    render() {
        const node = super.render();

        new GetGiftForm(node, getGiftFormTemplate);
    }
}