class Gift {
    constructor( { _id, title, minAge, maxAge, price, link, interests, description } ) {
        this._id = _id;
        this.title = title;
        this.minAge = minAge;
        this.maxAge = maxAge;
        this.price = price;
        this.link = link;
        this.interests = interests;
        this.description = description;
    }

    get data() {
        const data = { 
            title: this.title,
            minAge: this.minAge,
            maxAge: this.maxAge,
            price: this.price,
            link: this.link,
            interests: this.interests,
            description: this.description
        };

        return data;
    }

    set 
}

export default Gift;