export function Observable() {
    const observers = [];
    this.sendMessage = msg => {
        observers.map( obs => {
            obs.notify(msg);
        });
    };

    this.addObserver = ( observer ) => {
        observers.push( observer );
    };
}

export function Observer( behavior ) {
    this.notify = msg => behavior( msg ); 
}