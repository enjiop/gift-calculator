import ROUTES from "./routes";

export const initRoute = () => {
    const currentRoute = getCurrentRoute();
    renderRoute(currentRoute);
}

export const getRouteByPath = ( path ) => ROUTES.find(item => item.path === path);

export const getCurrentRoute = () => {
    const key = window.location.hash.replace('#', '');
    const route = getRouteByPath( key );

    return route;
}

export const renderRoute = (route) => {
    const root = document.getElementById('root');

    if (route !== undefined) {
        new route.component(root, route.title);
    } else {}
}

export const pushToHistory = (route) => {
    const hashedUrl = `${window.location.origin}/#${route.path}`;
    window.history.pushState(null, route.title, hashedUrl);
}

export const redirectTo = ( path ) => {
    const route = ROUTES.find( item => item.path === path);
    const hashedUrl = `${window.location.origin}/#${route.path}`;
    window.history.pushState( null, route.title, hashedUrl );
    renderRoute( route );
}