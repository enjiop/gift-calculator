import {
    HomePage,
    NewGiftPage,
    RecomendedGiftsPage,
    GetGiftPage
} from '../pages'

const ROUTES = [{
        path: '',
        component: HomePage,
        title: 'Список всех подарков'
    },
    {
        path: '/create',
        component: NewGiftPage,
        title: 'Создайте свой собственный подарок'
    },
    {
        path: '/recomended',
        component: RecomendedGiftsPage,
        title: 'Вот что мы можем посоветовать'
    },
    {
        path: '/getGift',
        component: GetGiftPage,
        title: 'Подберите подарок прямо сейчас'
    }
];

export default ROUTES;