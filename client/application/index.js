import {
    initRoute
} from './tools/router';

const app = () => {
    document.addEventListener('DOMContentLoaded', () => {
        initRoute();

        window.addEventListener('popstate', (e) => {
            initRoute();
        });
    });
};

export default app;