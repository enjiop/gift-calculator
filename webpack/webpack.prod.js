const path = require('path');
const common = require('./webpack.common.js');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const miniCssExtractPlugin = new MiniCssExtractPlugin({
    moduleFilename: ({ name }) => `${name.replace('/js/', '/css/')}.css`,
});

module.exports = merge(common, {
    output: {
        path: path.resolve(__dirname, '../public'),
        filename: 'js/[name].[contentHash].bundle.js'
    },
    mode: 'production',
    optimization: {
        minimizer: [
            new OptimizeCssAssetsPlugin(),
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        drop_console: true
                    }
                }
            })
        ]
    },
    module: {
        rules: [{
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            },
            {
                test: /\.sass$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        miniCssExtractPlugin,
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './client/assets/index.html',
            filename: 'index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true
            }
        }),
    ]
});